﻿using System;
using System.Reflection;
using uMod.Common;
using uMod.Extensions;

namespace uMod.Unity
{
    /// <summary>
    /// The extension class that represents this extension
    /// </summary>
    [EngineExtension(GameEngine.Unity)]
    public class UnityExtension : Extension
    {
        internal static Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static AssemblyName AssemblyName = Assembly.GetName();
        internal static VersionNumber AssemblyVersion = new VersionNumber(AssemblyName.Version.Major, AssemblyName.Version.Minor, AssemblyName.Version.Build);
        internal static string AssemblyAuthors = ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly, typeof(AssemblyCompanyAttribute), false)).Company;

        /// <summary>
        /// Gets whether this extension is a core extension
        /// </summary>
        public override bool IsCoreExtension => true;

        /// <summary>
        /// Gets the title of this extension
        /// </summary>
        public override string Title => "Unity";

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public override string Author => AssemblyAuthors;

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public override VersionNumber Version => AssemblyVersion;

        /// <summary>
        /// Gets the version of the game engine
        /// </summary>
        public string EngineVersion => UnityEngine.Application.unityVersion;

        /// <summary>
        /// Default engine-specific references for use in plugins
        /// </summary>
        public override string[] DefaultReferences => new[]
        {
            "UnityEngine",
            "UnityEngine.AIModule",
            "UnityEngine.AssetBundleModule",
            "UnityEngine.CoreModule",
            "UnityEngine.GridModule",
            "UnityEngine.ImageConversionModule",
            "UnityEngine.Networking",
            "UnityEngine.PhysicsModule",
            "UnityEngine.TerrainModule",
            "UnityEngine.TerrainPhysicsModule",
            "UnityEngine.UI",
            "UnityEngine.UIModule",
            "UnityEngine.UIElementsModule",
            "UnityEngine.UnityWebRequestAudioModule",
            "UnityEngine.UnityWebRequestModule",
            "UnityEngine.UnityWebRequestTextureModule",
            "UnityEngine.UnityWebRequestWWWModule",
            "UnityEngine.VehiclesModule",
            "UnityEngine.WebModule"
        };

        /// <summary>
        /// List of namespaces allowed for use in plugins
        /// </summary>
        public override string[] WhitelistedNamespaces => new[]
        {
            "UnityEngine"
        };

        /// <summary>
        /// Initializes a new instance of the UnityExtension class
        /// </summary>
        public UnityExtension() : base()
        {
        }

        /// <summary>
        /// Loads this extension
        /// </summary>
        public override void Load()
        {
            Interface.uMod.LogInfo($"Unity version: {UnityEngine.Application.unityVersion}");

            // Register the engine clock
            Interface.uMod.RegisterEngineClock(() => UnityScript.RealtimeSinceStartup);

            // Register our MonoBehaviour
            UnityScript.Create();
        }

        /// <summary>
        /// Replacement WWW class for Unity
        /// </summary>
        public static class WWW
        {
            [Obsolete("Use UnityWebRequest instead; a full-featured, more efficient replacement")]
            public static UnityEngine.WWW Create(string url)
            {
                if (new Uri(url).IsFile)
                {
                    throw new ArgumentException("Destination URL invalid. Path is restricted to remote URLs (http, https, ftp)");
                }

                return new UnityEngine.WWW(url);
            }
        }
    }
}
