﻿using System;
using System.Linq;
using uMod.Common;
using UnityEngine;
using LogLevel = uMod.Common.LogLevel;

namespace uMod.Unity
{
    /// <summary>
    /// Useful extension methods which are added to base types
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Converts a UnityEngine Vector3 to a universal Position
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Position ToPosition(this Vector3 pos) => new Position(pos.x, pos.y, pos.z);

        /// <summary>
        /// Converts a comma delimited string to a UnityEngine Vector3
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Vector3 ToVector3(this string input)
        {
            float[] split = input.Split(',', ' ').Select(x => Convert.ToSingle(x.Trim('(').Trim(')'))).ToArray();
            Vector3 result = Vector3.zero;
            if (split.Length > 0)
            {
                result.x = split[0];
            }
            if (split.Length > 1)
            {
                result.y = split[1];
            }
            if (split.Length > 2)
            {
                result.z = split[2];
            }
            return result;
        }

        /// <summary>
        /// Converts a universal Position to a UnityEngine Vector3
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Vector3 ToVector3(this Position pos) => new Vector3(pos.X, pos.Y, pos.Z);

        /// <summary>
        /// Converts a UnityEngine Vector2 to a universal Point
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Point ToPoint(this Vector2 pos) => new Point(pos.x, pos.y);

        /// <summary>
        /// Converts a comma delimited string to a UnityEngine Vector2
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Vector2 ToVector2(this string input)
        {
            float[] split = input.Split(',', ' ').Select(x => Convert.ToSingle(x.Trim('(').Trim(')'))).ToArray();
            Vector2 result = Vector2.zero;
            if (split.Length > 0)
            {
                result.x = split[0];
            }
            if (split.Length > 1)
            {
                result.y = split[1];
            }
            return result;
        }

        /// <summary>
        /// Converts a universal Point to a UnityEngine Vector3
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Vector2 ToVector2(this Point pos) => new Vector2(pos.X, pos.Y);

        /// <summary>
        /// Converts a universal Position4 to a UnityEngine Vector4
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Vector4 ToVector4(this Position4 pos) => new Vector4(pos.X, pos.Y, pos.Z, pos.T);

        /// <summary>
        /// Converts a UnityEngine Vector4 to a universal Position4
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static Position4 ToPosition4(this Vector4 pos) => new Position4(pos.x, pos.y, pos.z, pos.w);

        /// <summary>
        /// Converts a comma delimited string to a UnityEngine Vector4
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Vector4 ToVector4(this string input)
        {
            float[] split = input.Split(',', ' ').Select(x => Convert.ToSingle(x.Trim('(').Trim(')'))).ToArray();
            Vector4 result = Vector4.zero;
            if (split.Length > 0)
            {
                result.x = split[0];
            }
            if (split.Length > 1)
            {
                result.y = split[1];
            }
            if (split.Length > 2)
            {
                result.z = split[2];
            }
            if (split.Length > 3)
            {
                result.w = split[3];
            }
            return result;
        }

        /// <summary>
        /// Converts a UnityEngine LogType to the universal LogLevel format
        /// </summary>
        /// <param name="logType"></param>
        /// <returns></returns>
        public static LogLevel ToLogType(this LogType logType)
        {
            switch (logType)
            {
                case LogType.Assert:
                case LogType.Error:
                case LogType.Exception:
                    return LogLevel.Error;

                case LogType.Warning:
                    return LogLevel.Warning;

                case LogType.Log:
                default:
                    return LogLevel.Info;
            }
        }
    }
}
