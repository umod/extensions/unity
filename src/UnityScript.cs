﻿using System;
using System.Reflection;
using UnityEngine;

#pragma warning disable 0618

namespace uMod.Unity
{
    /// <summary>
    /// The main MonoBehaviour which calls uMod.OnFrame
    /// </summary>
    public class UnityScript : MonoBehaviour
    {
        public static GameObject Instance { get; private set; }
        public static float RealtimeSinceStartup { get; private set; }
        private uMod.Module uMod;

        public static void Create()
        {
            Instance = new GameObject("uMod.Unity");
            DontDestroyOnLoad(Instance);
            Instance.AddComponent<UnityScript>();
        }

        private void Awake()
        {
            RealtimeSinceStartup = Time.realtimeSinceStartup;
            uMod = Interface.uMod;
            //RegisterLogCallback();
        }

        private void RegisterLogCallback()
        {
            EventInfo eventInfo = typeof(UnityEngine.Application).GetEvent("logMessageReceived");
#if UNITY4
            if (eventInfo == null)
            {
                // Unity 4.x
                FieldInfo logCallbackField = typeof(UnityEngine.Application).GetField("s_LogCallback", BindingFlags.Static | BindingFlags.NonPublic);
                UnityEngine.Application.LogCallback logCallback = logCallbackField?.GetValue(null) as UnityEngine.Application.LogCallback;

                if (logCallback == null)
                {
                    Interface.uMod.LogWarning("No Unity application log callback is registered");
                }

                UnityEngine.Application.RegisterLogCallback((message, stackTrace, type) =>
                {
                    logCallback?.Invoke(message, stackTrace, type);
                    LogMessageReceived(message, stackTrace, type);
                });
            }
#else
            // Unity 5.0 and above
            Delegate handleException = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, "LogMessageReceived");
            eventInfo.GetAddMethod().Invoke(null, new object[] { handleException });
#endif
        }

        private void Update()
        {
            RealtimeSinceStartup = Time.realtimeSinceStartup;
            uMod.OnFrame(Time.deltaTime);
        }

        private void OnDestroy()
        {
            if (!uMod.IsShuttingDown)
            {
                uMod.LogWarning("The uMod Unity script was destroyed (creating a new instance)");
                uMod.NextTick(Create);
            }
        }

        private void OnApplicationQuit()
        {
            if (!uMod.IsShuttingDown)
            {
                Interface.Call("OnServerShutdown");
                Interface.uMod.OnShutdown();
            }
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        // This CANNOT be static! The world may impode if so, maybe worse
        private void LogMessageReceived(string message, string stackTrace, LogType type)
        {
            if (type == LogType.Exception)
            {
                //RemoteLogger.Exception(message, stackTrace);
            }
        }
    }
}
