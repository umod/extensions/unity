using System.Threading;
using uMod.Common;
using uMod.Logging;
using LogLevel = uMod.Common.LogLevel;

namespace uMod.Unity.Logging
{
    /// <summary>
    /// A logger that writes to the Unity console
    /// </summary>
    [Logger("unity")]
    public sealed class UnityLogger : ConsoleLogger
    {
        private readonly Thread mainThread = Thread.CurrentThread;

        /// <summary>
        /// Initializes a new instance of the UnityLogger class
        /// </summary>
        public UnityLogger() : base()
        {
        }

        /// <summary>
        /// Immediately writes a message to the Unity console
        /// </summary>
        /// <param name="message"></param>
        /// <param name="defaultMessage"></param>
        protected override void ProcessMessage(LogMessage message, string defaultMessage = null)
        {
            if (Thread.CurrentThread != mainThread)
            {
                Interface.uMod.NextTick(() => ProcessMessage(message, defaultMessage));
                return;
            }

            if (string.IsNullOrEmpty(defaultMessage))
            {
                defaultMessage = Format.Interpolate(message);
            }

            switch (message.Level)
            {
                case LogLevel.Alert:
                case LogLevel.Critical:
                case LogLevel.Emergency:
                case LogLevel.Error:
                    UnityEngine.Debug.LogError(defaultMessage);
                    break;

                case LogLevel.Notice:
                case LogLevel.Warning:
                    UnityEngine.Debug.LogWarning(defaultMessage);
                    break;

                case LogLevel.Debug:
                case LogLevel.Info:
                default:
                    UnityEngine.Debug.Log(defaultMessage);
                    break;
            }
        }
    }
}
